﻿using System;
using System.IO;
using iTextSharp.text.pdf;

namespace iTextSharpPDFUtility
{
    public class MyReader : PdfReader
    {
        public MyReader(String filename, byte[] userPwd) :
            base(filename, userPwd)
        { }
        public void DecryptOnPurpose()
        {
            encrypted = false;
        }
}
    static class Utility
    {
        public static Boolean UnlockFile(string FilePath, string originalFileName, string newFileName, string password)
        {
            try
            {
                MyReader.unethicalreading = true;

                using (MyReader reader = new MyReader(FilePath + "/" + originalFileName, System.Text.Encoding.ASCII.GetBytes(password)))
                {
                    using (Stream output = new FileStream(FilePath + "/" + newFileName, FileMode.CreateNew))
                    {
                        reader.DecryptOnPurpose();
                        //using (PdfStamper stamper = new PdfStamper(reader, output)) { }
                        PdfStamper stamper = new PdfStamper(reader, output);
                        AcroFields af = stamper.AcroFields;
                        af.SetField("test", "testValue");
                        stamper.FormFlattening = true;
                        stamper.Close();
                    }
                }
                File.Delete(FilePath + "/" + originalFileName);
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
    }
}
