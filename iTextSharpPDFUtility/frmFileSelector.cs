﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace iTextSharpPDFUtility
{
    public partial class frmFileSelector : Form
    {
        public frmFileSelector()
        {
            InitializeComponent();
            pbFileConversion.Visible = false;
        }

        private void btnPdfProcess_Click(object sender, EventArgs e)
        {
            pbFileConversion.Visible = true;
            pbFileConversion.Value = 0;
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                try
                {
                    Utility.UnlockFile(ibFolderPath.Text,
                            row.Cells[1].Value.ToString(),
                            row.Cells[2].Value.ToString(),
                            tbPassword.Text);
                    //Update the status cell
                    row.Cells[3].Value = @"Done";
                }
                catch(Exception exp)
                {
                    Console.WriteLine(exp.Message);
                    //Update the status cell
                    row.Cells[3].Value = exp.Message;
                }
                finally
                {
                    pbFileConversion.PerformStep();
                }
            }
            dataGridView.AutoResizeColumns();
            MessageBox.Show(@"Unlock Complete. Check status.");
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            pbFileConversion.Visible = false;
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                dialog.ShowNewFolderButton = false;
                //dialog.RootFolder = Environment.SpecialFolder.MyComputer;
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    ibFolderPath.Text = dialog.SelectedPath;
                    FileInfo[] files = new System.IO.DirectoryInfo(dialog.SelectedPath).GetFiles();

                    pbFileConversion.Minimum = 0;
                    pbFileConversion.Maximum = files.Length; 

                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[4] { new DataColumn("Index", typeof(int)),
                                new DataColumn("Original FileName", typeof(string)),
                                new DataColumn("New FileName",typeof(string)),
                                new DataColumn("Status",typeof(string)) });
                    int counter = 0;
                    string pattern = @"^(\d{8})(-)(\d{8})(.pdf)$";
                    Regex r = new Regex(pattern, RegexOptions.IgnoreCase);
                    foreach (FileInfo file in files)
                    {
                        Match m = r.Match(file.Name);
                        if(m.Success)
                        {
                            counter++;
                            string newName = m.Groups[1].ToString() + file.Extension;
                            dt.Rows.Add(counter, file.Name, newName);
                        }
                    }
                    dataGridView.DataSource = dt;
                    dataGridView.AutoResizeColumns();
                }
            }
        }
    }
}
