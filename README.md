# ezedox windows pdf utility

This is a .NET project that makes PDF sign ready by removing the security. It uses iTextSharp library and is governed by iText AGPL license.

Check here for more details : https://itextpdf.com/AGPL

## Steps to build this project

* Install iTextSharp from NuGet Manager

* After installation is complete, copy the dll to the iTextSharpPDFUtility

* Change the property of the dll for no copy and EmbeddedAssembly